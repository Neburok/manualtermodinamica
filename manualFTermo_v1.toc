\select@language {spanish}
\contentsline {chapter}{INTRODUCCI\IeC {\'O}N}{\es@scroman {iv}}
\contentsline {chapter}{\numberline {1}Conceptos B\IeC {\'a}sicos de Termodin\IeC {\'a}mica}{1}
\contentsline {section}{\numberline {1.1}Conceptos b\IeC {\'a}sicos}{1}
\contentsline {section}{\numberline {1.2}Propiedades termodin\IeC {\'a}micas}{1}
\contentsline {section}{\numberline {1.3}Leyes de los gases ideales}{1}
\contentsline {section}{\numberline {1.4}Sustancias puras}{1}
\contentsline {chapter}{\numberline {2}Primera Ley de la Termodin\IeC {\'a}mica}{2}
\contentsline {section}{\numberline {2.1}Introducci\IeC {\'o}n a la Primera ley de la termodin\IeC {\'a}mica}{2}
\contentsline {section}{\numberline {2.2}Balance de energ\IeC {\'\i }a en sistemas cerrados}{2}
\contentsline {section}{\numberline {2.3}Balance de energ\IeC {\'\i }a en sistemas abiertos}{2}
\contentsline {chapter}{\numberline {3}Segunda Ley de la Termodin\IeC {\'a}mica}{3}
\contentsline {section}{\numberline {3.1}Procesos Reversibles}{3}
\contentsline {section}{\numberline {3.2}Procesos Irreversibles}{3}
\contentsline {chapter}{\numberline {4}Ciclos Termodin\IeC {\'a}micos}{4}
\contentsline {section}{\numberline {4.1}Ciclo Otto}{4}
\contentsline {section}{\numberline {4.2}Ciclo Diesel}{4}
\contentsline {section}{\numberline {4.3}Ciclo Rankine}{4}
\contentsline {section}{\numberline {4.4}Ciclo de Refrigeraci\IeC {\'o}n}{4}
\contentsline {chapter}{\numberline {5}Mecanismos de transferencia de calor en estado estable}{5}
\contentsline {section}{\numberline {5.1}Conducci\IeC {\'o}n}{5}
\contentsline {section}{\numberline {5.2}Convecci\IeC {\'o}n}{5}
\contentsline {section}{\numberline {5.3}Radiaci\IeC {\'o}n}{5}
\contentsline {section}{\numberline {5.4}Circuitos t\IeC {\'e}rmicos: Analog\IeC {\'\i }a el\IeC {\'e}ctrica en paredes planas}{5}
